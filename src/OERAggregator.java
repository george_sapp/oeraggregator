import java.io.UnsupportedEncodingException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.*;
import com.betterknow.oera.common.OERMaterial;
import com.betterknow.oera.connectors.coursera.*;
import com.betterknow.oera.io.*;
import com.esotericsoftware.yamlbeans.YamlWriter;

import org.yaml.snakeyaml.Yaml;

public class OERAggregator {

	public static void main(String[] args) {
		
		//load content into output
		JSONRoot output = new JSONRoot();
		
		try {
			
			Coursera c = new Coursera();
				while(c.hasNext()){
					output.materials.add(c.next());
					output.total_records++;
					System.out.println(c.getPercentComplete() + "%");
				}
				System.out.println(output.total_records);
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		//output to file YAMLBeans
		try {

			YamlWriter writer = new YamlWriter(new FileWriter("YAMLBeans.yml"));
			
			writer.getConfig().setClassTag("OERMaterial", OERMaterial.class);
			writer.getConfig().setClassTag("OERA", JSONRoot.class);

			writer.getConfig().writeConfig.setEscapeUnicode(true);
			
			writer.write(output);
			writer.close();
			writer = null;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*
		//Not usable for large objects > 100k records
		//output to file SnakeYAML		
		try {
			
			Yaml yml = new Yaml();
			yml.dump(output, new FileWriter("SnakeYAML.yml"));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		
	}

}
