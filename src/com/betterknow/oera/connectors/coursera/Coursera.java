package com.betterknow.oera.connectors.coursera;

import com.betterknow.oera.common.IConnector;
import com.betterknow.oera.common.IDPrefixs;
import com.betterknow.oera.common.MaterialTypes;
import com.betterknow.oera.common.OERMaterial;
import com.google.gson.*;

import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;







import javax.net.ssl.HttpsURLConnection;

import com.google.gson.*;
import com.google.gson.internal.*;

import java.util.HashMap;
import java.util.Set;
import java.util.Collection;
import java.util.List;
import java.util.AbstractMap;
import java.util.Iterator;

/**
 * @author George Sapp
 * 
 *ToDo: retrieve subject/discipline information and use map class to setup map
 */
public class Coursera implements IConnector{
	
	//Constants
	private static final String get_topics_path = "https://www.coursera.org/maestro/api/topic/list2";
	private static final String get_topic_detail_path = "https://www.coursera.org/maestro/api/topic/information?topic-id=";
	private static final String course_url_prefix = "https://www.coursera.org/course/";
	private static final String map_file_path = "coursera_map.yml";
	
	//Stores JSON string of get list2 call
	private String list2JSON;
	
	//Stores the total number of topics
	private int numberTopics;
	private int currTopicIndex;
	
	Iterator<AbstractMap> topicValueIterator;
	
	public Coursera() throws UnsupportedEncodingException {

		
		//get all coursera courses
		this.list2JSON = makeRequest(this.get_topics_path);
		
		//deserialize the response
		this.deserializeList2(this.list2JSON);
		
	}
	
	private void deserializeList2(String JSONResult){
		//deserialize the response
		Gson g = new Gson();
		LinkedTreeMap root = g.fromJson(JSONResult, LinkedTreeMap.class);
		LinkedTreeMap topics = (LinkedTreeMap) root.get("topics");		
		Collection<AbstractMap> topicValues = topics.values();
		//store iterator in global variable
		this.topicValueIterator = topicValues.iterator();
		
		this.numberTopics = topicValues.size();
		this.currTopicIndex = 0;
	}
	
	//Provides a method to test structure of list2JSON return
	public HashMap testGetTopicsJSON(){
		Gson g = new Gson();
		
		HashMap map = g.fromJson(this.list2JSON, HashMap.class);
		
		return map;
		
	}
	
	public boolean hasNext(){
		return this.topicValueIterator.hasNext();
	}
	
	public OERMaterial next() throws UnsupportedEncodingException{
		
		OERMaterial mat = new OERMaterial();
		mat.source = "Coursera";
		
		AbstractMap topic = topicValueIterator.next();
		
		//start filling out the material's fields
		String shortName = (String)topic.get("short_name");
		
		//get material details
		LinkedTreeMap topicDetails = getTopicDetails(shortName);
		
		
		mat.description_long = (String)topicDetails.get("about_the_course");
		mat.description = (String)topicDetails.get("short_description");
		mat.detailUrl = (String)topicDetails.get("preview_link");
		mat.isbn13 = IDPrefixs.Coursera.toString() + ":" + shortName;
		mat.url = this.course_url_prefix+ shortName;
		mat.image = (String)topic.get("large_icon");
		mat.title = (String)topic.get("name");
		mat.type = MaterialTypes.course.toString();
		
		//increment current index value for percentage complete calculation
		this.currTopicIndex++;
		
		return mat;
		
	}
	
	public int getPercentComplete(){
		return ((this.currTopicIndex*100)/this.numberTopics);
	}
	
	private LinkedTreeMap getTopicDetails(String topicShortName) throws UnsupportedEncodingException{
		
		String JSONResponse = makeRequest(get_topic_detail_path + topicShortName);
		
		Gson g = new Gson();
		return g.fromJson(JSONResponse, LinkedTreeMap.class);
		
		
	}
	
	public int getNumberTopics(){
		return this.numberTopics;
	}
	
	public void resetIterator(){
		this.deserializeList2(this.list2JSON);
	}
	
	//actually makes the http request
	private String makeRequest(String address) throws UnsupportedEncodingException {
	     URL url;
	     //path = URLEncoder.encode(path, "UTF-8");
	     try {
	        
		        
			    url = new URL(address);
			    HttpsURLConnection con = (HttpsURLConnection)url.openConnection();

		 
	            BufferedReader br = 
	            		new BufferedReader(
	            			new InputStreamReader(con.getInputStream()));
	             
	            	String input;
	            	StringBuilder b = new StringBuilder();
	            	while ((input = br.readLine()) != null){
	            	   b.append(input);
	            	}
	            	br.close();
	            	
	            	return b.toString();
	 
		 } catch (MalformedURLException e) {
			e.printStackTrace();
		 } catch (IOException e) {
			e.printStackTrace();
		 }
	     
	     return "Failed";
	}
}
