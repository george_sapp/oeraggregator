package com.betterknow.oera.io;

import com.betterknow.oera.common.OERMaterial;

import java.util.ArrayList;
import java.util.Date;

public class JSONRoot {

	public int total_records;
	public Date date_created;
	
	public ArrayList<OERMaterial> materials;
	
	public JSONRoot() {		
		total_records = 0;
		date_created = new Date();
		materials = new ArrayList<OERMaterial>();
	}

}
