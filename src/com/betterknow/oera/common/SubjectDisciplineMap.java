package com.betterknow.oera.common;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.betterknow.oera.io.SubjectDisciplineMapData;
import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlReader;

/**
 * @author George
 * Provides a class which allows subject/discipline to be mapped between API values and a file which
 */
public class SubjectDisciplineMap {
	
	private static final String noMapFoundResult = "";
	
	private String mapFile;
	private SubjectDisciplineMapData data;
	
	public SubjectDisciplineMap(String mapFile) throws FileNotFoundException, YamlException {
		//store in global
		this.mapFile = mapFile;
		
		//parse YAML file into MapData
		YamlReader Yaml = new YamlReader(new FileReader(mapFile));
		data = Yaml.read(SubjectDisciplineMapData.class);
		
	}
	
	public int getNumberSubjects(){return data.Subject.size();}
	public int getNumberDisciplines(){return data.Discipline.size();}
	
	public String mapSubject(String key){
		if (data.Subject.containsKey(key))
			return data.Subject.get(key);
		else
			return noMapFoundResult;
	}
	
	public String mapDiscipline(String key){
		if(data.Discipline.containsKey(key))
			return data.Discipline.get(key);
		else
			return noMapFoundResult;
	}
	
}
