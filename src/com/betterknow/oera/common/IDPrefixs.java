package com.betterknow.oera.common;

public enum IDPrefixs {
	Coursera("CR");
	private final String identifier;
	
	private IDPrefixs(String identifier)
    {
        this.identifier = identifier;
    }
	
	 public String toString()
    {
        return identifier;
    }
}
