package com.betterknow.oera.common;

import java.io.UnsupportedEncodingException;


public interface IConnector {
	
	//Called to get the next element
	public OERMaterial next() throws UnsupportedEncodingException;
	
	//returns true while there is another OERmaterial to return
	public boolean hasNext();
	
	//returns the estimated percent
	public int getPercentComplete();

}
