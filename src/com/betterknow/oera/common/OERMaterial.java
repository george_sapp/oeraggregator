package com.betterknow.oera.common;

import java.util.ArrayList;

/**
 * @author George Sapp
 * @category Common DataTypes
 *
 */
public class OERMaterial {

	/*
	 * Important Cases of Values
	 *  
	 * The �isbn13� attribute
	 * If the value of the attribute is prefixed with one of the known prefixes (currently "YT:", "ML:", and "CT:"), for example "YT:cNKHsP6iWMY", "ML:77297", then the value is treated as an id for a specific source and type of the course material. Otherwise, the course material is treated a book.
	 * 
	 * The �source� attribute
	 * A missing �source� attribute value is equal to an empty value and it means that the material source is not important for all the practical reasons. 
	 * 
	 * The �owner� attribute
	 * A missing �owner� attribute value is equal to an empty string array (array of owners) and means that the material access permission are always �public�
	 * 
	 * The �type� attribute
	 * A missing �type� attribute value is equal to an empty value and it means that the material is a book (or, one can say, that the material is a book for all the practical reason from viewpoint of the application)
	 */
	
	//Primary (mandatory) unique identifier for a course material. Such as youtubeID
	public String isbn13;
	
	//Secondary (optional) unique identifier for a course material. 
	public String isbn10;
	
	//Material title or name
	public String title;
	
	//Authors of a course material, values of the attribute must be in a form of a string array.
	public ArrayList<String> author;
	
	//A text value of a date of publishing, not normalized, but always must start to with 4 digits representing a year of publishing. 
	//I.E. "2010"
	public String pubdate;
	
	//I.E. 1
	public String edition;
	
	//imprint
	public String impn;
	
	//A description of a course material provided by the publisher
	public String description;
	
	//A more elaborated description of a course material provided by the publisher
	public String description_long;
	
	//Metlot Discipline ID.  Must be mapped from other data using a map
	public String discipline_id;
	
	//Merlot subject ID.  Must be mapped from other data using a map
	public String subject_id;
	
	//course material community rating (instructor rating)
	public int rating;
	
	//reference to the first edition of a material
	public String family;
	
	//Synthetic sales for an entire family of course materials
	public float sales_a;
	
	//Total (new and used) sales
	public float sales_t;
	
	//New units sold
	public float sales_nu;
	
	//Sales of new units
	public float sales_n;
	
	//price of new units
	public float price_n;
	
	//Used units sold
	public float sales_uu;
	
	//Sales of used units
	public float sales_u;
	
	//price of used units
	public float price_u;
	
	//where did this material come from?  I.E. youtube, nature.com, coursera, etc
	public String source;
	
	//who owns this material.  Company name, etc
	public String[] owner;
	
	//URL to details about this material
	public String detailUrl;
	
	//URL to an image of the content
	public String image;
	
	//URL pointing to the material
	public String url;
	
	//I.E. video, book, paper, etc
	public String type;
	
	
	
	public OERMaterial() {
		//set some default values here
		
		author = new ArrayList<String>();
		
	}
	
	/*
	public void setISBN13(String isbn13){this.isbn13 = isbn13;}
	public String getISBN13(){return this.isbn13;}
	
	public void setTitle(String title){this.title = title;}
	public String getTitle(){return this.title;}
	
	public void addAuthor(String author){this.author.add(author);}
	
	public void setPubDate(String pubdate){this.pubdate = pubdate;}
	
	public void setEdition (String edition){this.edition = edition;}
	
	public void setPublisher(String impn){this.impn = impn;}
	
	public void setDescription(String description){this.description = description;}
	
	public void setDescriptionLong(String description_long){this.description_long = description_long;}
	
	public void setDisciplineID(String discipline_id){this.discipline_id = discipline_id;}
	
	public void setSubjectID(String subject_id){this.subject_id = subject_id;}
	
	public void setRating(int rating){this.rating = rating;}
	
	public void setPriceNew(float price_n){this.price_n = price_n;}
	
	public void setPriceUsed(float price_u){this.price_u = price_u;}
	
	public void setSource(String source){this.source = source;}
	
	public void setDetailURL(String detailUrl){this.detailUrl = detailUrl;}
	
	public void setImageURL(String image){this.image = image;}
	public String getImageURL(){return this.image;}
	
	public void setURL(String url){this.url = url;}
	public String getURL(){return this.url;}
	*/
	

}
