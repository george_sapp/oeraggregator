package com.betterknow.oera.test;

import static org.junit.Assert.*;

import java.io.UnsupportedEncodingException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.betterknow.oera.connectors.coursera.Coursera;
import com.google.gson.internal.LinkedTreeMap;
import com.betterknow.oera.common.OERMaterial;

import java.util.HashMap;

public class TestCoursera {

	//minimum number of topics we can see before test fails
	private static final int min_num_topics = 300;
	
	private Coursera c;
	
	@Before
	public void setUp() throws Exception {
		this.c = new Coursera();
	}

	@After
	public void tearDown() throws Exception {
		this.c = null;
	}
	
	@Test
	public void testHasNext() throws UnsupportedEncodingException {
		//make sure it has at least two records available
		assertTrue("Should have first material", c.hasNext());
		c.next();
		assertTrue("Should have second material", c.hasNext());
	}
	
	@Test
	//make sure every material has a minimal set of fields.
	public void testMaterial() throws UnsupportedEncodingException {
		
		c.resetIterator();
		
		System.out.println("IDs of all Coursera Materials:");
		
		while(c.hasNext()){
			OERMaterial mat = c.next();
			System.out.print(mat.isbn13 + ":: ");
			assertTrue(mat.isbn13.length() > 0);
			assertTrue(mat.image.length() > 0);
			assertTrue(mat.url.length() > 0);
			assertTrue(mat.title.length() > 0);
			System.out.print(mat.url + "\n");
			
		}
	}
	
	@Test
	//that that there is a topic node in the list2 JSON response
	public void testTopicFormat() throws Exception{
		
		HashMap map = c.testGetTopicsJSON();
		
		assertTrue("Should contain topics key in root of JSON response", map.containsKey("topics"));
		
		
	}
	
	@Test
	public void testNumberTopics() throws Exception{
		System.out.println("Total Coursera Materials: " + c.getNumberTopics());
		assertTrue("All topics should be returned",c.getNumberTopics() > min_num_topics);
	}
	

}
