package com.betterknow.oera.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import com.betterknow.oera.common.SubjectDisciplineMap;
import com.betterknow.oera.io.JSONRoot;
import com.betterknow.oera.io.SubjectDisciplineMapData;
import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlWriter;

public class TestSubjectDisciplineMap {
	
	//Constants
	private static final String test_file_path = "testMap.yml";
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testGenerateData(){
		
		SubjectDisciplineMapData data = new SubjectDisciplineMapData();
		
		for (int i = 1; i < 1000; i ++)
			data.Discipline.put(String.valueOf(i), String.valueOf(i*10-5));
		
		for (int i = 1; i < 500; i ++)
			data.Subject.put(String.valueOf(i), String.valueOf(i*10-5));
		
		
		
		try {
			YamlWriter writer;
			writer = new YamlWriter(new FileWriter(test_file_path));
			writer.write(data);
			writer.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test
	//loads the test data we generated and checks number of records
	public void testLoadData() throws FileNotFoundException, YamlException{
		SubjectDisciplineMap map = new SubjectDisciplineMap(test_file_path);
		
		assertTrue("Should be 999 Disciplines", map.getNumberDisciplines() == 999);
		assertTrue("Should be 499 Subjects", map.getNumberSubjects() == 499);
	}
	
	@Test
	//tests mapping Subject and Discipline against test data
	public void testSpecificMap() throws FileNotFoundException, YamlException{
		
		SubjectDisciplineMap map = new SubjectDisciplineMap(test_file_path);

		assertTrue("338 maps to 3375", map.mapSubject("338").equals("3375"));
		assertTrue("942 maps to 9415", map.mapDiscipline("942").equals("9415"));
		
	}

}
